﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Infrastructure.Repositories
{
    public class BaseRepository<TEntity, TypeId> : IRepository<TEntity, TypeId>
        where TEntity : BaseEntity<TypeId>
    {
        private readonly SecureFlightDbContext _context;

        public BaseRepository(SecureFlightDbContext context)
        {
            _context = context;
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(TypeId id)
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(e => e.Id.Equals(id));
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return entity;
        }
    }
}

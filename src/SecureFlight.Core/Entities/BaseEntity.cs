﻿namespace SecureFlight.Core.Entities
{
    public class BaseEntity <TypeId>
    {
        public TypeId Id { get; set; }
    }
}

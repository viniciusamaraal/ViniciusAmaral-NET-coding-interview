﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecureFlight.Core.Entities
{
    public class Airport : EntityStringId
    {
        public Airport()
        {
            this.OriginFlights = new HashSet<Flight>();
            this.DestinationFlights = new HashSet<Flight>();
        }

        public string Name { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public ICollection<Flight> OriginFlights { get; set; }

        public ICollection<Flight> DestinationFlights { get; set; }
    }
}

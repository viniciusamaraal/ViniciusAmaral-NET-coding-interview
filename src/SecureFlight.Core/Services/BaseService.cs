﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class BaseService<TEntity, TypeId> : IService<TEntity, TypeId>
        where TEntity : BaseEntity<TypeId>
    {
        private readonly IRepository<TEntity, TypeId> _repository;

        public BaseService(IRepository<TEntity, TypeId> repository)
        {
            _repository = repository;
        }

        public async Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync()
        {
            return new OperationResult<IReadOnlyList<TEntity>>(await _repository.GetAllAsync());
        }
    }
}

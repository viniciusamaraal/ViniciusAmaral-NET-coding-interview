﻿using SecureFlight.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IRepository<TEntity, TypeId>
        where TEntity : BaseEntity<TypeId>
    {
        Task<IReadOnlyList<TEntity>> GetAllAsync();

        Task<TEntity> GetByIdAsync(TypeId id);

        Task<TEntity> UpdateAsync(TEntity entity);
    }
}

﻿using SecureFlight.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IService<TEntity, TypeId>
        where TEntity : BaseEntity<TypeId>
    {
        Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync();
    }
}

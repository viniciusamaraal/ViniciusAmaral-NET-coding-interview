﻿namespace SecureFlight.Api.Models
{
    public class AirportDataTransferObject
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}
